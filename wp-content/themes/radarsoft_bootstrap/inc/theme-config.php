<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "radar_fd_option";

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Theme Options', 'radar_fd' ),
        'page_title'           => __( 'Theme Options', 'radar_fd' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        'footer_credit'     => ' ',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'system_info'          => false,
        // REMOVE

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );
	Redux::setArgs( $opt_name, $args );


	Redux::setSection( $opt_name, array(
        'title' => __( 'Basic Setting', 'radar_fd' ),
        'id'    => 'basic',
        'icon'  => 'el el-home'
    ) );

	Redux::setSection( $opt_name, array(
        'title'      => __( 'Logo & Favicon', 'radar_fd' ),
        'id'         => 'basic-logo',
        'subsection' => true,
		'fields'     => array(
			array(
				'id'       	=> 'logo',
				'type'     	=> 'media',
				'url'      	=> true,
				'title'    	=> __('Logo', 'radar_fd'),
				'desc'     	=> '',
				'subtitle' 	=> '',
				'default'  	=> array(
					'url'			=>''
				),
			),
            array(
				'id'       	=> 'logomobile',
				'type'     	=> 'media',
				'url'      	=> true,
				'title'    	=> __('Logo for Mobile', 'radar_fd'),
				'desc'     	=> '',
				'subtitle' 	=> '',
				'default'  	=> array(
					'url'			=>''
				),
			),
			array(
				'id'       	=> 'favicon',
				'type'     	=> 'media',
				'url'      	=> true,
				'title'    	=> __('Favicon', 'radar_fd'),
				'desc'     	=> __('File type .ico size 16px * 16px', 'radar_fd'),
				'subtitle' 	=> '',
				'default'  	=> array(
					'url'		=>''
				),
			),
			array(
				'id'       	=> 'no-img',
				'type'     	=> 'media',
				'url'      	=> true,
				'title'    	=> __('No image', 'radar_fd'),
				'desc'     	=> '',
				'subtitle' 	=> '',
				'default'  	=> array(
					'url'		=>''
				),
			),
		)
	) );

    Redux::setSection( $opt_name, array(
        'title' => __( 'Header & Top menu', 'radar_fd' ),
        'id'    => 'header-topmenu',
        'icon'  => 'el el-lines'
    ) );

    Redux::setSection( $opt_name, array(
        'title' => __( 'Search icon', 'radar_fd' ),
        'id'    => 'top-search-icon',
		'subsection' => true,
		'fields'=> array(
			array(
				'id'	=> 'enabled-top-search-icon',
				'type'	=> 'switch',
				'title'	=> __('Enabled Search icon', 'radar_fd'),
				'default' => true,
			),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title' => __( 'Footer', 'radar_fd' ),
        'id'    => 'footer',
        'icon'  => 'el el-lines'
    ) );

    Redux::setSection( $opt_name, array(
        'title' => __( 'Copyright', 'radar_fd' ),
        'id'    => 'Footer copyright',
		'subsection' => true,
		'fields'=> array(
			array(
				'id'	=> 'enabled-footer-copyright',
				'type'	=> 'switch',
				'title'	=> __('Enabled footer copyright', 'radar_fd'),
				'default' => true,
			),
            array(
                'id'    => 'text-footer-copyright',
                'type'  => 'editor',
                'title' => __('Copyright', 'radar_fd'),
                'default'   => '',
                'args'  => array(
                    'teeny' => true,
                    'textarea_rows' => 10
                )
			),
            array(
                'id'       => 'to-top',
                'type'     => 'text',
                'title'    => __('Text to top button', 'radar_fd'),
                'default'  => 'Top'
            ),
        )
    ) );
    /*
	Redux::setSection( $opt_name, array(
        'title' => __( 'Typography & Style', 'radar_fd' ),
        'id'    => 'typography',
        'icon'  => 'el el-font'
    ) );

	Redux::setSection( $opt_name, array(
        'title' => __( 'Top Nav Menu', 'radar_fd' ),
        'id'    => 'tnv-typography',
		'subsection' => true,
		'fields'=> array(
			array(
				'id'	=> 'enabled-typography',
				'type'	=> 'switch',
				'title'	=> __('Enabled Typography', 'radar_fd'),
				'default' => false,
			),
			array(
				'id'	=> 'bg-site-header',
				'type'	=> 'background',
				'title'	=> __('Header BG', 'radar_fd'),
				'compiler'	=> array( '.site-header' ),
			),
			array(
				'id'	=> 'tnv1-typography',
				'type'	=> 'typography',
				'title'	=> __('Menu', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-header ul.primary_menu > li > a' ),
				'font-backup' => true,
				'text-align' => true,
				'text-transform' => true,
			),
			array(
				'id'	=> 'tnv1-typography-hover',
				'type'	=> 'typography',
				'title'	=> __('Menu Hover & Current', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '
								.site-header ul.primary_menu > li:hover > a,
								.site-header ul.primary_menu > li.current_page_item > a,
								.site-header ul.primary_menu > li.current_page_parent > a,
								.site-header ul.primary_menu > li.current-menu-ancestor > a,
								.site-header ul.primary_menu > li.current-cat > a,
								.site-header ul.primary_menu > ul > li.current-menu-item > a
							' ),
				'font-backup' 	=> false,
				'font-style'	=> false,
				'font-weight'	=> false,
				'font-size'		=> false,
				'font-family'	=> false,
				'line-height'	=> false,
				'text-align'	=> false,
			),
			array(
				'id'	=> 'tnv2-typography',
				'type'	=> 'typography',
				'title'	=> __('Sub Menu', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-header ul.primary_menu ul.sub-menu li a' ),
				'font-backup' => true,
				'text-align' => true,
				'text-transform' => true,
			),
			array(
				'id'	=> 'tnv2-typography-bg',
				'type'	=> 'background',
				'title'	=> __('Sub Menu BG', 'radar_fd'),
				'compiler'	=> array( '.site-header ul.primary_menu ul.sub-menu li a' ),
				'background-repeat'	=> false,
				'background-attachment'	=> false,
				'background-position'	=> false,
				'background-image'	=> false,
				'background-size'	=> false,
				'preview'	=> false,
			),
			array(
				'id'	=> 'tnv2-typography-hover',
				'type'	=> 'typography',
				'title'	=> __('Sub Menu Hover & Current', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '
								.site-header ul.primary_menu ul.sub-menu li a:hover,
								.site-header ul.primary_menu ul.sub-menu li.current-menu-item a
							' ),
				'font-backup' 	=> false,
				'font-style'	=> false,
				'font-weight'	=> false,
				'font-size'		=> false,
				'font-family'	=> false,
				'line-height'	=> false,
				'text-align'	=> false,
			),
			array(
				'id'	=> 'tnv2-typography-hover-bg',
				'type'	=> 'background',
				'title'	=> __('Sub Menu Hover & Current BG', 'radar_fd'),
				'compiler'	=> array( 'ul.top-menu ul.sub-menu li a:hover, ul.top-menu ul.sub-menu li.current-menu-item a' ),
				'background-repeat'	=> false,
				'background-attachment'	=> false,
				'background-position'	=> false,
				'background-image'	=> false,
				'background-size'	=> false,
				'preview'	=> false,
			),
		),
	) );

	Redux::setSection( $opt_name, array(
        'title' => __( 'Content', 'radar_fd' ),
        'id'    => 'content-typography',
		'subsection' => true,
		'fields'=> array(
			array(
				'id'	=> 'content-entry-title',
				'type'	=> 'typography',
				'title'	=> __('Content Entry Title', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-content .entry-title' ),
				'font-backup' => true,
				'text-align' => true,
			),
			array(
				'id'	=> 'site-content-h1',
				'type'	=> 'typography',
				'title'	=> __('Content H1', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-content h1' ),
				'font-backup' => true,
				'text-align' => false,
			),
			array(
				'id'	=> 'site-content-h2',
				'type'	=> 'typography',
				'title'	=> __('Content H2', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-content h2' ),
				'font-backup' => true,
				'text-align' => false,
			),
			array(
				'id'	=> 'site-content-h3',
				'type'	=> 'typography',
				'title'	=> __('Content H3', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-content h3' ),
				'font-backup' => true,
				'text-align' => false,
			),
			array(
				'id'	=> 'site-content-h4',
				'type'	=> 'typography',
				'title'	=> __('Content H4', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-content h4' ),
				'font-backup' => true,
				'text-align' => false,
			),
			array(
				'id'	=> 'site-content-h5',
				'type'	=> 'typography',
				'title'	=> __('Content H5', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-content h5' ),
				'font-backup' => true,
				'text-align' => false,
			),
			array(
				'id'	=> 'site-content-h6',
				'type'	=> 'typography',
				'title'	=> __('Content H6', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-content h6' ),
				'font-backup' => true,
				'text-align' => false,
			),
			array(
				'id'	=> 'site-content-p',
				'type'	=> 'typography',
				'title'	=> __('Content p', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-content p' ),
				'font-backup' => true,
				'text-align' => false,
			),
			array(
				'id'	=> 'site-content-link',
				'type'	=> 'typography',
				'title'	=> __('Content Link color', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-content a' ),
				'font-backup' 	=> false,
				'font-style'	=> false,
				'font-weight'	=> false,
				'font-size'		=> false,
				'font-family'	=> false,
				'line-height'	=> false,
				'text-align'	=> false,
			),
			array(
				'id'	=> 'site-content-linkhover',
				'type'	=> 'typography',
				'title'	=> __('Content Link hover color', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-content a:hover' ),
				'font-backup' 	=> false,
				'font-style'	=> false,
				'font-weight'	=> false,
				'font-size'		=> false,
				'font-family'	=> false,
				'line-height'	=> false,
				'text-align'	=> false,
			),
		),
    ) );

	Redux::setSection( $opt_name, array(
        'title' => __( 'Footer', 'radar_fd' ),
        'id'    => 'footer-typography',
		'subsection' => true,
		'fields'=> array(
			array(
				'id'	=> 'bg-site-footer',
				'type'	=> 'background',
				'title'	=> __('Footer BG', 'radar_fd'),
				'compiler'	=> array( '.site-footer' ),
			),
			array(
				'id'	=> 'site-footer-h1',
				'type'	=> 'typography',
				'title'	=> __('Footer H1', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-footer h1' ),
				'font-backup' => true,
				'text-align' => false,
			),
			array(
				'id'	=> 'site-footer-h2',
				'type'	=> 'typography',
				'title'	=> __('Footer H2', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-footer h2' ),
				'font-backup' => true,
				'text-align' => false,
			),
			array(
				'id'	=> 'site-footer-h3',
				'type'	=> 'typography',
				'title'	=> __('Footer H3', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-footer h3' ),
				'font-backup' => true,
				'text-align' => false,
			),
			array(
				'id'	=> 'site-footer-h4',
				'type'	=> 'typography',
				'title'	=> __('Footer H4', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-footer h4' ),
				'font-backup' => true,
				'text-align' => false,
			),
			array(
				'id'	=> 'site-footer-h5',
				'type'	=> 'typography',
				'title'	=> __('Footer H5', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-footer h5' ),
				'font-backup' => true,
				'text-align' => false,
			),
			array(
				'id'	=> 'site-footer-h6',
				'type'	=> 'typography',
				'title'	=> __('Footer H6', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-footer h6' ),
				'font-backup' => true,
				'text-align' => false,
			),
			array(
				'id'	=> 'site-footer-p',
				'type'	=> 'typography',
				'title'	=> __('Footer p', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-footer p' ),
				'font-backup' => true,
				'text-align' => false,
			),
			array(
				'id'	=> 'site-footer-link',
				'type'	=> 'typography',
				'title'	=> __('Link color', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-footer a' ),
				'font-backup' 	=> false,
				'font-style'	=> false,
				'font-weight'	=> false,
				'font-size'		=> false,
				'font-family'	=> false,
				'line-height'	=> false,
				'text-align'	=> false,
			),
			array(
				'id'	=> 'site-footer-linkhover',
				'type'	=> 'typography',
				'title'	=> __('Link hover color', 'radar_fd'),
				'google'   => true,
				'compiler'	=> array( '.site-footer a:hover' ),
				'font-backup' 	=> false,
				'font-style'	=> false,
				'font-weight'	=> false,
				'font-size'		=> false,
				'font-family'	=> false,
				'line-height'	=> false,
				'text-align'	=> false,
			),
		),
	) );
	*/
    /*
	Redux::setSection( $opt_name, array(
        'title' => __( 'Archive', 'radar_fd' ),
        'id'    => 'archive-page',
		'subsection' => false,
		'fields'=> array(
			array(
				'id'	=> 'archive-layout',
				'type'	=> 'image_select',
				'title'	=> __('Archive Layout', 'radar_fd'),
				'options' => array(
					'full_width' => array(
						'alt' => 'Full width',
						'img' => ReduxFramework::$_url.'assets/img/1col.png'
					),
					'sidebar_left' => array(
						'alt' => 'Sidebar Left',
						'img' => ReduxFramework::$_url.'assets/img/2cl.png'
					),
					'sidebar_right' => array(
						'alt' => 'Sidebar Right',
						'img' => ReduxFramework::$_url.'assets/img/2cr.png'
					),
				),
				'default' => 'sidebar_left',
			),
		),
	) );
	*/
	Redux::setSection( $opt_name, array(
        'title'      => __( 'ACE Editor', 'radar_fd' ),
        'id'         => 'ace-editor',
        'icon'  => 'el el-icon-cogs',
        'subsection' => false,
        'desc'       => '',
        'fields'     => array(
            array(
                'id'       => 'ace-editor-js-header',
                'type'     => 'ace_editor',
                'title'    => __( 'Header JS Code', 'radar_fd' ),
                'subtitle' => __( 'Paste your JS code here.', 'radar_fd' ),
                'mode'     => 'javascript',
                'theme'    => 'monokai',
                'desc'     => '',
                'default'  => ''
            ),
        )
    ) );

	function compiler_action($options, $css, $changed_values) {
		global $wp_filesystem;

		#$filename = dirname(__FILE__) . '/theme_options.css';
		$filename = get_template_directory() . '/css/theme_options.css';

		if( empty( $wp_filesystem ) ) {
			require_once( ABSPATH .'wp-admin/includes/file.php' );
			WP_Filesystem();
		}

		if( $wp_filesystem ) {
			$wp_filesystem->put_contents(
				$filename,
				$css,
				FS_CHMOD_FILE // predefined mode settings for WP files
			);
		}

	}
	add_filter('redux/options/'.$opt_name.'/compiler', 'compiler_action', 10, 3);

	function radar_fd_themeoption_scripts(){
		global $radar_fd_option;
		if($radar_fd_option['enabled-typography']==1){
			if( file_exists(get_template_directory().'/css/theme_options.css') ){
				wp_enqueue_style('radar_fd_themeoption',get_template_directory_uri().'/css/theme_options.css');
			}
		}
	}
	add_action('wp_enqueue_scripts', 'radar_fd_themeoption_scripts');

	function removeDemoModeLink() { // Be sure to rename this function to something more unique
		if ( class_exists('ReduxFrameworkPlugin') ) {
			remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
		}
		if ( class_exists('ReduxFrameworkPlugin') ) {
			remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
		}
	}
	add_action('init', 'removeDemoModeLink');


?>
