<?php
get_header(); ?>
	<div class="container">
		<h2><?php printf( __( 'Search Results for: %s', 'radar_fd' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
		<?php get_template_part('loops/loop', 'list'); ?>
	</div>
<?php get_footer(); ?>
