<form role="search" method="get" class="search-form form-inline" action="<?php echo home_url( '/' ); ?>">
	<div class="form-group">
		<input type="search" class="search-field form-control" placeholder="<?php echo __( 'Search', 'radar_fd' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo __( 'Search for:', 'radar_fd' ) ?>" />
		<input type="submit" class="search-submit btn btn-default" value="<?php echo __( 'Search', 'radar_fd' ) ?>" />
	</div>
</form>
