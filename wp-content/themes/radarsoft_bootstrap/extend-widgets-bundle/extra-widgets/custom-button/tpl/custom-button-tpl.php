<?php
	if(!$instance['button_url']){
		$instance['button_url']='#';
	}
	if($instance['button_new_window']){
		$target = 'target="_blank"';
	}
?>
<div class="custom-button">
	<a href="<?php echo $instance['button_url']; ?>" <?php echo $target; ?>>
		<?php echo $instance['button_text'] ?>
	</a>
</div>