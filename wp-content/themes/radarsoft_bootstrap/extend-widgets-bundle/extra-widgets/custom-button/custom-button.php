<?php
/*
Widget Name: Custom Button
Description:
Author: Pond
Author URI:
*/

class Custom_button_theme_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'custom-button-theme-widget',
			__('Custom button', 'custom-widget-bundle'),
			array(
				'description' => __('Custom Button', 'custom-widget-bundle'),
				'help'        => '',
			),
			array(

			),
			false,
			plugin_dir_path(__FILE__)
		);
	}

	function get_widget_form() {
		return array(
			'button_text' => array(
				'type' => 'text',
				'label' => __('Button text', 'custom-widget-bundle'),
				'default' => ''
			),
			'button_url' => array(
				'type' => 'text',
				'label' => __('Destination URL', 'custom-widget-bundle'),
				'default' => ''
			),
			'button_new_window' => array(
				'type' => 'checkbox',
				'label' => __( 'Open in a new window', 'custom-widget-bundle' ),
				'default' => false
			),
			'button_style' => array(
				'type' => 'section',
				'label' => __( 'Button Style' , 'widget-form-fields-text-domain' ),
				'hide' => true,
				'fields' => array(
					'button_style_text_color' => array(
						'type' => 'color',
						'label' => __( 'Text color', 'widget-form-fields-text-domain' )
					),
					'button_style_bg_color' => array(
						'type' => 'color',
						'label' => __( 'Background color', 'widget-form-fields-text-domain' )
					),
					'button_style_border_color' => array(
						'type' => 'color',
						'label' => __( 'Border color', 'widget-form-fields-text-domain' )
					),
					'button_style_padding_top_bottom' => array(
        		'type' => 'slider',
        		'label' => __( 'Choose a number padding top and buttom', 'widget-form-fields-text-domain' ),
        		'default' => 15,
        		'min' => 15,
        		'max' => 50,
        		'integer' => true
    			),
					'button_style_padding_left_right' => array(
        		'type' => 'slider',
        		'label' => __( 'Choose a number padding top and buttom', 'widget-form-fields-text-domain' ),
        		'default' => 20,
        		'min' => 20,
        		'max' => 80,
        		'integer' => true
    			)
				)
			),
			'button_hover_style' => array(
				'type' => 'section',
				'label' => __( 'Button Hover Style' , 'widget-form-fields-text-domain' ),
				'hide' => true,
				'fields' => array(
					'button_hoverstyle_text_color' => array(
						'type' => 'color',
						'label' => __( 'Text color', 'widget-form-fields-text-domain' )
					),
					'button_hoverstyle_color' => array(
						'type' => 'color',
						'label' => __( 'Background color', 'widget-form-fields-text-domain' )
					),
					'button_hoverstyle_border_color' => array(
						'type' => 'color',
						'label' => __( 'Border color', 'widget-form-fields-text-domain' )
					)
				)
			)
		);
	}

    function get_template_name($instance) {
		return 'custom-button-tpl';
	}

	function get_template_dir($instance) {
		return 'tpl';
	}

	function get_style_name($instance) {
		return 'custom-button';
	}

	function get_less_variables( $instance ) {
		$less_value = array();
		$less_value['button_style_text_color'] = $instance['button_style']['button_style_text_color'];
		$less_value['button_style_bg_color'] = $instance['button_style']['button_style_bg_color'];
		if(isset($instance['button_style']['button_style_border_color'])){
			$less_value['button_style_border_size'] = '1px';
		}else{
			$less_value['button_style_border_size'] = '0px';
		}
		$less_value['button_style_border_color'] = $instance['button_style']['button_style_border_color'];
		$less_value['button_style_padding_top_bottom'] = $instance['button_style']['button_style_padding_top_bottom'].'px';
		$less_value['button_style_padding_left_right'] = $instance['button_style']['button_style_padding_left_right'].'px';

		$less_value['button_hoverstyle_text_color'] = $instance['button_hover_style']['button_hoverstyle_text_color'];
		$less_value['button_hoverstyle_color'] = $instance['button_hover_style']['button_hoverstyle_color'];
		$less_value['button_hoverstyle_border_color'] = $instance['button_hover_style']['button_hoverstyle_border_color'];

		return $less_value;
	}
}

siteorigin_widget_register('custom-button-theme-widget', __FILE__, 'Custom_button_theme_Widget');
?>
