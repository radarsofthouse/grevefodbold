<?php
/*
Widget Name: Jobs Position
Description: 
Author: Pond
Author URI: 
*/

class Custom_jobs_theme_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'custom-jobs-theme-widget',
			__('Jobs Position', 'custom-widget-bundle'),
			array(
				'description' => __('Jobs Position', 'custom-widget-bundle'),
				'help'        => '',
			),
			array(
			
			),
			false,
			plugin_dir_path(__FILE__)
		);
	}
	
	function initialize() {
		$this->register_frontend_styles(
			array(
				array(
					'jobs-position',
					get_template_directory_uri().'/extend-widgets-bundle/extra-widgets/jobs-position/styles/jobs-position.css', 
					array(), 
					'1.0'
				),
			)
		);
	}
	
	function get_widget_form() {
		return array(
			'job_repeater' => array(
				'type' => 'repeater',
				'label' => __( 'Job repeater widget bundle' , 'custom-widget-bundle' ),
				'item_name'  => __( 'Repeater item', 'custom-widget-bundle' ),
				'item_label' => array(
					'selector'     => "[id*='job_title']",
					'update_event' => 'change',
					'value_method' => 'val'
				),
				'fields' => array(
					'job_title' => array(
						'type' => 'text',
						'label' => __('Job Title', 'custom-widget-bundle'),
						'default' => ''
					),
					'job_description' => array(
						'type' => 'tinymce',
						'label' => __('Job description', 'custom-widget-bundle'),
						'default' => '',
						'rows' => 10,
						'default_editor' => 'html',
						'button_filters' => array(
							'mce_buttons' => array( $this, 'filter_mce_buttons' ),
							'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
							'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
							'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
							'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
						),
					),
					'job_experience' => array(
						'type' => 'tinymce',
						'label' => __('Wanted experience and competencies', 'custom-widget-bundle'),
						'default' => '',
						'rows' => 10,
						'default_editor' => 'html',
						'button_filters' => array(
							'mce_buttons' => array( $this, 'filter_mce_buttons' ),
							'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
							'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
							'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
							'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
						),
					),
					'job_skills' => array(
						'type' => 'tinymce',
						'label' => __('Language skills', 'custom-widget-bundle'),
						'default' => '',
						'rows' => 10,
						'default_editor' => 'html',
						'button_filters' => array(
							'mce_buttons' => array( $this, 'filter_mce_buttons' ),
							'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
							'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
							'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
							'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
						),
					),
					'job_salary' => array(
						'type' => 'tinymce',
						'label' => __('Salary', 'custom-widget-bundle'),
						'default' => '',
						'rows' => 5,
						'default_editor' => 'html',
						'button_filters' => array(
							'mce_buttons' => array( $this, 'filter_mce_buttons' ),
							'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
							'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
							'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
							'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
						),
					),
					'job_moreinfo' => array(
						'type' => 'tinymce',
						'label' => __('More information', 'custom-widget-bundle'),
						'default' => '',
						'rows' => 5,
						'default_editor' => 'html',
						'button_filters' => array(
							'mce_buttons' => array( $this, 'filter_mce_buttons' ),
							'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
							'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
							'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
							'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
						),
					),
					'job_workplace' => array(
						'type' => 'select',
						'label' => __( 'Work Place', 'custom-widget-bundle' ),
						'prompt' => __( 'Choose a Work Place', 'custom-widget-bundle' ),
						'options' => array(
							'Copenhagen' => __( 'Copenhagen', 'custom-widget-bundle' ),
							'Chiang Mai' => __( 'Chiang Mai', 'custom-widget-bundle' ),
						)
					),
					'job_type' => array(
						'type' => 'select',
						'label' => __( 'Job type', 'custom-widget-bundle' ),
						'prompt' => __( 'Choose a Job type', 'custom-widget-bundle' ),
						'options' => array(
							'Full­time' => __( 'Full­time', 'custom-widget-bundle' ),
							'Part time' => __( 'Part time', 'custom-widget-bundle' ),
							'Internship' => __( 'Internship', 'custom-widget-bundle' ),
						)
					),
					'job_start_date' => array(
						'type' => 'text',
						'label' => __('Expected start date', 'custom-widget-bundle'),
						'default' => ''
					),
					'job_deadline' => array(
						'type' => 'text',
						'label' => __('Application deadline', 'custom-widget-bundle'),
						'default' => ''
					),
					'job_submit_form' => array(
						'type' => 'tinymce',
						'label' => __('Submit your application form', 'custom-widget-bundle'),
						'default' => '',
						'rows' => 5,
						'default_editor' => 'html',
						'button_filters' => array(
							'mce_buttons' => array( $this, 'filter_mce_buttons' ),
							'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
							'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
							'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
							'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
						),
					),
					'job_tellfriend_form' => array(
						'type' => 'tinymce',
						'label' => __('Tell a friend form', 'custom-widget-bundle'),
						'default' => '',
						'rows' => 5,
						'default_editor' => 'html',
						'button_filters' => array(
							'mce_buttons' => array( $this, 'filter_mce_buttons' ),
							'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
							'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
							'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
							'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
						),
					),
				),
			)
		);
	}

    function get_template_name($instance) {
		return 'jobs-position';
	}

	function get_template_dir($instance) {
		return 'tpl';
	}
	
	function get_style_name($instance) {
		return 'jobs-position';
	}
	
	function get_less_variables( $instance ) {
		
	}
}

siteorigin_widget_register('custom-jobs-theme-widget', __FILE__, 'Custom_jobs_theme_Widget');
?>