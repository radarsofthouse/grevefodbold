<?php
if(!empty($instance['job_repeater'])){
	$repeater_items = $instance['job_repeater'];
	?>
	<div id="accordion-<?php echo $instance['panels_info']['widget_id']; ?>" class="job-accordion" role="tablist" aria-multiselectable="true">
	<?php
	$count_row = 1;
	foreach($repeater_items as $index => $repeater_item ){
		if(!empty($repeater_item)){
			?>
			<div class="card job-position panel">
				<div class="job-header" role="tab" id="heading-<?php echo $count_row; ?>">
					<a class="collapsed" data-toggle="collapse" href="#collapse-<?php echo $count_row; ?>" aria-expanded="false" aria-controls="collapse-<?php echo $count_row; ?>" data-parent="#accordion-<?php echo $instance['panels_info']['widget_id']; ?>">
						<?php
						if(!empty($repeater_item['job_title'])){
							echo $repeater_item['job_title'];
						}
						?>
					</a>
				</div>
				<div id="collapse-<?php echo $count_row; ?>" class="job-description-collapse collapse <?php if($count_row==1){ echo 'show';} ?>" role="tabpanel" aria-labelledby="heading-<?php echo $count_row; ?>">
					<div class="job-description-box">
						<div class="job-content">
							<?php
							if(!empty($repeater_item['job_description'])){
								?>
									<div class="job-description">
										<h4><?php _e('Job description', 'custom-widget-bundle'); ?></h4>
										<?php
										echo $repeater_item['job_description'];
										?>
									</div>
								<?php
							}
							if(!empty($repeater_item['job_experience'])){
								?>
									<div class="job-experience">
										<h4><?php _e('Wanted experience and competencies', 'custom-widget-bundle'); ?></h4>
										<?php
										echo $repeater_item['job_experience'];
										?>
									</div>
								<?php
							}
							if(!empty($repeater_item['job_skills'])){
								?>
									<div class="job-skills">
										<h4><?php _e('Language skills', 'custom-widget-bundle'); ?></h4>
										<?php
										echo $repeater_item['job_skills'];
										?>
									</div>
								<?php
							}
							if(!empty($repeater_item['job_salary'])){
								?>
									<div class="job-salary">
										<h4><?php _e('Salary', 'custom-widget-bundle'); ?></h4>
										<?php
										echo $repeater_item['job_salary'];
										?>
									</div>
								<?php
							}
							if(!empty($repeater_item['job_moreinfo'])){
								?>
									<div class="job-information">
										<h4><?php _e('More information', 'custom-widget-bundle'); ?></h4>
										<?php
										echo $repeater_item['job_moreinfo'];
										?>
									</div>
								<?php
							}
							?>
							<div class="job-last-section">
								<?php
								if(!empty($repeater_item['job_workplace'])){
									echo sprintf(__('<p><label>Work Place :</label> %s</p>','custom-widget-bundle'),$repeater_item['job_workplace']);
								}
								if(!empty($repeater_item['job_type'])){
									echo sprintf(__('<p><label>Job type :</label> %s</p>','custom-widget-bundle'),$repeater_item['job_type']);
								}
								if(!empty($repeater_item['job_start_date'])){
									echo sprintf(__('<p><label>Expected start date :</label> %s</p>','custom-widget-bundle'),$repeater_item['job_start_date']);
								}
								if(!empty($repeater_item['job_deadline'])){
									echo sprintf(__('<p><label>Application deadline :</label> %s</p>','custom-widget-bundle'),$repeater_item['job_deadline']);
								}
								?>
							</div>
							<div class="job-submit-form">
							<?php
							if(!empty($repeater_item['job_submit_form'])){
								?>
								<div class="job-position-modal-dialog">
								<!-- Button trigger modal -->
								<div class="modal-button">
									<button type="button" class="submit-job-form" data-toggle="modal" data-target="#subit-form-modal-<?php echo $count_row; ?>">
										<?php _e('Submit your application', 'custom-widget-bundle'); ?>
									</button>
								</div>

								<!-- Modal -->
								<div class="modal fade" id="subit-form-modal-<?php echo $count_row; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $repeater_item['panels_info']['widget_id']; ?>_subit_form" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<?php echo $repeater_item['job_submit_form']; ?>
											</div>
										</div>
									</div>
								</div>
								</div>
								<?php
							}
							if(!empty($repeater_item['job_tellfriend_form'])){
								?>
								<div class="job-position-modal-dialog">
								<!-- Button trigger modal -->
								<div class="modal-button">
									<button type="button" class="tellfriend-job-form" data-toggle="modal" data-target="#tellfriend-form-modal-<?php echo $count_row; ?>">
										<?php _e('Tell a friend', 'custom-widget-bundle'); ?>
									</button>
								</div>

								<!-- Modal -->
								<div class="modal fade" id="tellfriend-form-modal-<?php echo $count_row; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $repeater_item['panels_info']['widget_id']; ?>_tellfriend_form" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<?php echo $repeater_item['job_tellfriend_form']; ?>
											</div>
										</div>
									</div>
								</div>
								</div>
								<?php
							}
							?>
							</div>
						</div>
					</div>
				</div>
			  </div>
			<?php
		}
		$count_row++;
	}
	?>
	</div>
	<script>
	jQuery('.job-description-collapse').on('shown.bs.collapse', function (e) {
		var $panel = jQuery(this).closest('.job-position');
		jQuery('html,body').animate({
			scrollTop: $panel.offset().top -50
		}, 500); 
	});
	</script>
	<?php
}