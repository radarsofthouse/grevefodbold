<?php
/*
Widget Name: Modal Bootstrap
Description: 
Author: Pond
Author URI: 
*/

class Custom_modal_theme_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'custom-modal-theme-widget',
			__('Modal Bootstrap', 'custom-widget-bundle'),
			array(
				'description' => __('Custom Modal Bootstrap', 'custom-widget-bundle'),
				'help'        => '',
			),
			array(
			
			),
			false,
			plugin_dir_path(__FILE__)
		);
	}
	
	function get_widget_form() {
		return array(
			'modal_title' => array(
				'type' => 'text',
				'label' => __('Modal Title', 'custom-widget-bundle'),
				'default' => ''
			),
			'modal_content' => array(
				'type' => 'tinymce',
				'label' => __('Modal Content', 'custom-widget-bundle'),
				'default' => '',
				'rows' => 10,
				'default_editor' => 'html',
				'button_filters' => array(
					'mce_buttons' => array( $this, 'filter_mce_buttons' ),
					'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
					'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
					'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
					'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
				),
			),
			'modal_button_text' => array(
				'type' => 'text',
				'label' => __('Modal Button text', 'custom-widget-bundle'),
				'default' => ''
			),
			'modal_button_style' => array(
				'type' => 'section',
				'label' => __( 'Modal Button Style' , 'widget-form-fields-text-domain' ),
				'hide' => true,
				'fields' => array(
					'modal_button_style_text_color' => array(
						'type' => 'color',
						'label' => __( 'Text color', 'widget-form-fields-text-domain' )
					),
					'modal_button_style_bg_color' => array(
						'type' => 'color',
						'label' => __( 'Background color', 'widget-form-fields-text-domain' ),
						'default' => '#ffffff'
					),
					'modal_button_style_border_color' => array(
						'type' => 'color',
						'label' => __( 'Border color', 'widget-form-fields-text-domain' )
					)
				)
			),
			'modal_button_hover_style' => array(
				'type' => 'section',
				'label' => __( 'Modal Button Hover Style' , 'widget-form-fields-text-domain' ),
				'hide' => true,
				'fields' => array(
					'modal_button_hoverstyle_text_color' => array(
						'type' => 'color',
						'label' => __( 'Text color', 'widget-form-fields-text-domain' )
					),
					'modal_button_hoverstyle_color' => array(
						'type' => 'color',
						'label' => __( 'Background color', 'widget-form-fields-text-domain' )
					),
					'modal_button_hoverstyle_border_color' => array(
						'type' => 'color',
						'label' => __( 'Border color', 'widget-form-fields-text-domain' )
					)
				)
			)
		);
	}

    function get_template_name($instance) {
		return 'modal-tpl';
	}

	function get_template_dir($instance) {
		return 'tpl';
	}
	
	function get_style_name($instance) {
		return 'modal-style';
	}
	
	function get_less_variables( $instance ) {
		$less_value = array();
		$less_value['modal_button_text_color'] = $instance['modal_button_style']['modal_button_style_text_color'];
		if($instance['modal_button_style']['modal_button_style_bg_color']!=''){
			$less_value['modal_button_bg_color'] = $instance['modal_button_style']['modal_button_style_bg_color'];
		}else{
			$less_value['modal_button_bg_color'] = '#ffffff';
		}
		if($instance['modal_button_style']['modal_button_style_border_color']!=''){
			$less_value['modal_button_border_size'] = '1px';
		}else{
			$less_value['modal_button_border_size'] = '0px';
		}
		$less_value['modal_button_style_border_color'] = $instance['modal_button_style']['modal_button_style_border_color'];
		
		$less_value['modal_button_hoverstyle_text_color'] = $instance['modal_button_hover_style']['modal_button_hoverstyle_text_color'];
		$less_value['modal_button_hoverstyle_color'] = $instance['modal_button_hover_style']['modal_button_hoverstyle_color'];
		$less_value['modal_button_hoverstyle_border_color'] = $instance['modal_button_hover_style']['modal_button_hoverstyle_border_color'];
		return $less_value;
	}
}

siteorigin_widget_register('custom-modal-theme-widget', __FILE__, 'Custom_modal_theme_Widget');
?>