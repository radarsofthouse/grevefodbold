<?php
if(!empty($instance['modal_button_text'])){
	?>
	<div class="extend-modal-dialog">
	<!-- Button trigger modal -->
	<div class="modal-button">
		<button type="button" data-toggle="modal" data-target="#<?php echo $instance['panels_info']['widget_id']; ?>">
			<?php echo $instance['modal_button_text']; ?>
		</button>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="<?php echo $instance['panels_info']['widget_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $instance['panels_info']['widget_id']; ?>" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><?php echo $instance['modal_title']; ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<?php echo $instance['modal_content']; ?>
				</div>
			</div>
		</div>
	</div>
	</div>
	<?php
}
?>