<?php
if(!empty($instance['owl_carousel_repeater'])){
	?>
	<div id="owl_carousel_<?php echo $instance['panels_info']['widget_id']; ?>" class="owl-carousel owl-theme">
		<?php
		foreach($instance['owl_carousel_repeater'] as $index => $carousel){
			?>
			<div>
				<?php
					if($carousel['owl_carousel_link']!=''){
						$owl_carousel_link = $carousel['owl_carousel_link'];
						?>
						<a href="<?php echo $owl_carousel_link; ?>">
						<?php
					}
					if($carousel['owl_carousel_image']!=0){
						echo wp_get_attachment_image($carousel['owl_carousel_image'],'full');
					}
					if($carousel['owl_carousel_link']!=''){
						?>
						</a>
						<?php
					}
				?>
			</div>
			<?php
		}
		?>
	</div>
	<script type="text/javascript" language="javascript">
	jQuery(function() {
		jQuery('#owl_carousel_<?php echo $instance['panels_info']['widget_id']; ?>').owlCarousel({
			margin:20,
			navText:['&#60;','&#62;'],
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					nav:true,
					dots:false,
					loop:false,
				},
				600:{
					items:3,
					nav:true,
					dots:false,
					loop:false,
				},
				1000:{
					items:5,
					nav:false,
					dots:false,
					loop:false,
				}
			}
		});
	})
	</script>
	<?php
}
?>
