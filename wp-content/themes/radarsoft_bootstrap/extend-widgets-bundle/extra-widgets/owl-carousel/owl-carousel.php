<?php
/*
Widget Name: Carousel by Owl
Description:
Author: Pond
Author URI:
*/

class owl_carousel_theme_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'owl-carousel-theme-widget',
			__('Carousel', 'custom-widget-bundle'),
			array(
				'description' => __('Carousel by Owl', 'custom-widget-bundle'),
				'help'        => '',
			),
			array(

			),
			false,
			plugin_dir_path(__FILE__)
		);
	}

	function initialize() {
		$this->register_frontend_scripts(
			array(
				array(
					'owl-carousel',
					get_template_directory_uri().'/extend-widgets-bundle/extra-widgets/owl-carousel/js/owl.carousel.min.js',
					array( 'jquery' ),
					'2.2.1'
				),
			)
		);
		$this->register_frontend_styles(
			array(
				array(
					'owl-carousel',
					get_template_directory_uri().'/extend-widgets-bundle/extra-widgets/owl-carousel/styles/owl.carousel.min.css',
					array(),
					'2.2.1'
				),
				array(
					'owl-carousel-theme',
					get_template_directory_uri().'/extend-widgets-bundle/extra-widgets/owl-carousel/styles/owl.theme.default.min.css',
					array(),
					'2.2.1'
				),
				array(
					'owl-carousel-tpl',
					get_template_directory_uri().'/extend-widgets-bundle/extra-widgets/owl-carousel/styles/owl-carousel-tpl.css',
					array(),
					'1.0'
				),
			)
		);
	}

	function get_widget_form() {
		return array(
			'owl_carousel_repeater' => array(
				'type' => 'repeater',
				'label' => __( 'Carousel repeater.' , 'custom-widget-bundle' ),
				'item_name'  => __( 'Carousel item', 'custom-widget-bundle' ),
				'item_label' => array(
					'selector'     => "[id*='owl_carousel_title']",
					'update_event' => 'change',
					'value_method' => 'val'
				),
				'fields' => array(
					'owl_carousel_title' => array(
                		'type' => 'text',
                		'label' => __( 'Carousel image title', 'custom-widget-bundle' )
            		),
					'owl_carousel_image' => array(
						'type' => 'media',
						'label' => __( 'Carousel image', 'custom-widget-bundle' ),
						'choose' => __( 'Choose carousel image', 'custom-widget-bundle' ),
						'update' => __( 'Set image', 'custom-widget-bundle' ),
						'library' => 'image',
						'fallback' => true
					),
					'owl_carousel_link' => array(
						'type' => 'link',
						'label' => __('Carousel URL goes here', 'custom-widget-bundle'),
						'default' => ''
					)
				)
			)
		);
	}

	function get_template_name($instance) {
		return 'owl-carousel-tpl';
	}

	function get_template_dir($instance) {
		return 'tpl';
	}
}

siteorigin_widget_register('owl-carousel-theme-widget', __FILE__, 'owl_carousel_theme_Widget');
?>
