<?php
/*
Widget Name: Box with arrow
Description: 
Author: Pond
Author URI: 
*/

class Custom_box_arrow_theme_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'custom-box-arrow-theme-widget',
			__('Box Arrow', 'custom-widget-bundle'),
			array(
				'description' => __('Box Arrow', 'custom-widget-bundle'),
				'help'        => '',
			),
			array(
			
			),
			false,
			plugin_dir_path(__FILE__)
		);
	}

	function initialize() {
		$this->register_frontend_styles(
			array(
				array(
					'box-arrow',
					get_template_directory_uri().'/extend-widgets-bundle/extra-widgets/box-arrow/styles/box-arrow.css', 
					array(), 
					'1.0'
				),
			)
		);
	}
	
	function get_widget_form() {
		return array(
			'box_arrow_repeater' => array(
				'type' => 'repeater',
				'label' => __( 'Box arrow widget bundle' , 'custom-widget-bundle' ),
				'item_name'  => __( 'Repeater item', 'custom-widget-bundle' ),
				'item_label' => array(
					'selector'     => "[id*='box_arrow_title']",
					'update_event' => 'change',
					'value_method' => 'val'
				),
				'fields' => array(
					'box_arrow_title' => array(
						'type' => 'text',
						'label' => __('Title', 'custom-widget-bundle'),
						'default' => ''
					),
					'box_arrow_text' => array(
						'type' => 'tinymce',
						'label' => __( 'Text editor area', 'custom-widget-bundle' ),
						'default' => '',
						'rows' => 10,
						'default_editor' => 'tinymce',
						'button_filters' => array(
							'mce_buttons' => array( $this, 'filter_mce_buttons' ),
							'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
							'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
							'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
							'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
						),
					),
					'box_arrow_type' => array(
				        'type' => 'select',
				        'label' => __( 'Arrow type', 'custom-widget-bundle' ),
				        'default' => 'arrow_left',
				        'options' => array(
				            'arrow_left' => __( 'Arrow Left', 'custom-widget-bundle' ),
				            'arrow_right' => __( 'Arrow Right', 'custom-widget-bundle' ),
				            'arrow_bottom' => __( 'Arrow Bottom', 'custom-widget-bundle' ),
				            'arrow_none' => __( 'None', 'custom-widget-bundle' ),
				        )
				    ),
				),
			),
			'box_arrow_setting' => array(
				'type' => 'section',
				'label' => __( 'Box arrow setting' , 'custom-widget-bundle' ),
				'hide' => true,
				'fields' => array(
					'box_arrow_column' => array(
						'type' => 'slider',
						'label' => __( 'Box arrow column', 'custom-widget-bundle' ),
						'default' => 1,
        				'min' => 1,
        				'max' => 12,
        				'integer' => true,
					),
				)
			),
		);
	}

    function get_template_name($instance) {
		return 'box-arrow-tpl';
	}

	function get_template_dir($instance) {
		return 'tpl';
	}
	/*
	function get_style_name($instance) {
		return false;
	}
	
	function get_less_variables( $instance ) {
		/*return $less_value;
		return false;
	}
	*/
}

siteorigin_widget_register('custom-box-arrow-theme-widget', __FILE__, 'Custom_box_arrow_theme_Widget');
?>