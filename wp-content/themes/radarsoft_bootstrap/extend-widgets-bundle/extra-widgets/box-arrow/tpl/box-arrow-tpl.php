<?php
if(!empty($instance['box_arrow_repeater'])){
	$box_arrow_column = $instance['box_arrow_setting']['box_arrow_column'];
	$column = 12 / $box_arrow_column;
	$class_column = "col-xs-$column col-sm-$column col-md-$column col-lg-$column";
	$repeater_items = $instance['box_arrow_repeater'];
	?>
	<div class="row box-arrow">
		<?php
		$time_delay = 0.3;
		foreach($repeater_items as $index => $repeater_item ){
			?>
			<div class="<?php echo $class_column; ?> box-arrow-item wow fadeInDown" data-wow-offset="100" data-wow-iteration="1" data-wow-delay="<?php echo $time_delay; ?>s">
				<?php echo $repeater_item['box_arrow_text']; ?>
				<?php
					if($repeater_item['box_arrow_type']!='arrow_none'){
						?><div class="<?php echo $repeater_item['box_arrow_type']; ?>"></div><?php
					}
				?>
			</div>
			<?php
			$time_delay += 0.3;
		}
		?>
	</div>
	<?php
};
?>