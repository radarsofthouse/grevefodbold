<?php
/*
echo '<pre>';
print_r($instance['panels_info']['widget_id']);
echo '</pre>';
*/
if(!empty($instance['cases_repeater'])){
	/*============================== Cases for desktop */
	?>
	<div class="hidden-md-down">
	<ul id="<?php echo $instance['panels_info']['widget_id']; ?>_desktop" class="case-carouFredSel">
	<?php
	$c_row = 1;
	$delay = 0;
	foreach($instance['cases_repeater'] as $index => $case){
		if($c_row==1){
			echo '<li>';
		}
		?>
		<div class="case col-xs-1 col-sm-1 col-md-3">
			<?php
			if($case['cases_link']!=''){
				$case_url = $case['cases_link'];
				?>
				<a href="<?php echo $case_url; ?>"><span class="seemorecase"><?php _e('Se case','radar_fd'); ?></span></a>
				<?php
			}
			if($case['cases_bg']!=0){
			$case_image_bg = wp_get_attachment_image_src($case['cases_bg'],'full');
				$case_style_bg = 'style="background-image:url('.$case_image_bg[0].');"';
			}else{
				$case_style_bg = '';
			}
			?>
			<div class="case-image-box"><div class="case-image" <?php echo $case_style_bg; ?>></div></div>
			<div class="case-detail">
				<div class="logo">
					<?php
					if($case['cases_logo']!=0){
						echo wp_get_attachment_image($case['cases_logo'],'full');
					}
					?>
				</div>
				<div class="title"><?php echo $case['cases_title']; ?></div>
			</div>
		</div>
		<?php
		if($c_row==8){
			echo '</li>';
			$c_row=1;
		}else{
			$c_row++;
		}
	}
	if($c_row!=1){
		echo '</li>';
	}
	?>
	</ul>
	<div class="clearfix"></div>
	<div id="<?php echo $instance['panels_info']['widget_id']; ?>_desktop_pager" class="cases-pager"></div>
	<script type="text/javascript" language="javascript">
		jQuery(function() {
			$w_width = jQuery(window).width();
			jQuery('#<?php echo $instance['panels_info']['widget_id']; ?>_desktop li').width($w_width);
			jQuery('#<?php echo $instance['panels_info']['widget_id']; ?>_desktop').carouFredSel({
				responsive: true,
				auto: false,
				pagination: "#<?php echo $instance['panels_info']['widget_id']; ?>_desktop_pager",
				width: '100%',
				scroll: 1,
				items: {
					width: 400,
					visible: {
						min: 1,
						max: 1
					}
				}
			});
		});
		
		jQuery( window ).bind("resize", function(){
			$w_width = jQuery(window).width();
			jQuery('#<?php echo $instance['panels_info']['widget_id']; ?>_desktop li').width($w_width);
		});
	</script>
	</div>
	<?php
	/*============================== Cases for desktop */
	
	/*============================== Cases for mobile and tablet */
	?>
	<div class="hidden-lg-up case-mobile">
	<ul id="<?php echo $instance['panels_info']['widget_id']; ?>_mobile" class="case-carouFredSel">
	<?php
	$c_row = 1;
	$delay = 0;
	foreach($instance['cases_repeater'] as $index => $case){
		?>
		</li>
		<div class="case col-xs-12 col-sm-6 col-md-6">
			<?php
			if($case['cases_link']!=''){
				$case_url = $case['cases_link'];
			}else{
				$case_url = '#';
			}
			?>
			<?php /*<a href="<?php echo $case_url; ?>"></a>*/ ?>
			<?php
			if($case['cases_bg']!=0){
			$case_image_bg = wp_get_attachment_image_src($case['cases_bg'],'full');
				$case_style_bg = 'style="background-image:url('.$case_image_bg[0].');"';
			}else{
				$case_style_bg = '';
			}
			?>
			<div class="case-image-box"><div class="case-image" <?php echo $case_style_bg; ?>></div></div>
			<div class="case-detail">
				<div class="logo">
					<?php
					if($case['cases_logo']!=0){
						echo wp_get_attachment_image($case['cases_logo'],'full');
					}
					?>
				</div>
				<div class="title"><?php echo $case['cases_title']; ?></div>
			</div>
		</div>
		</li>
		<?php
	}
	?>
	</ul>
	<div class="clearfix"></div>
	<a id="prev_<?php echo $instance['panels_info']['widget_id']; ?>" class="cases-mobile-prev" href="#">
		<i class="fa fa-angle-left" aria-hidden="true"></i>
	</a>
	<a id="next_<?php echo $instance['panels_info']['widget_id']; ?>" class="cases-mobile-next" href="#">
		<i class="fa fa-angle-right" aria-hidden="true"></i>
	</a>
	<script type="text/javascript" language="javascript">
		jQuery(function() {
			$w_width = jQuery(window).width();
			jQuery('#<?php echo $instance['panels_info']['widget_id']; ?>_mobile li').width($w_width);
			jQuery('#<?php echo $instance['panels_info']['widget_id']; ?>_mobile').carouFredSel({
				responsive: true,
				auto: false,
				prev: '#prev_<?php echo $instance['panels_info']['widget_id']; ?>',
				next: '#next_<?php echo $instance['panels_info']['widget_id']; ?>',
				width: '100%',
				scroll: 1,
				items: {
					width: 400,
					visible: {
						min: 1,
						max: 1
					}
				},
				mousewheel: false,
				swipe: {
					onMouse: true,
					onTouch: true
				}
			});
		});
		
		jQuery( window ).bind("resize", function(){
			$w_width = jQuery(window).width();
			jQuery('#<?php echo $instance['panels_info']['widget_id']; ?>_mobile li').width($w_width);
		});
	</script>
	</div>
	<?php
	/*============================== Cases for mobile and tablet */
}
?>