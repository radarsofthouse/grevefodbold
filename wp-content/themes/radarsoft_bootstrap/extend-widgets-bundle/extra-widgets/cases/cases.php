<?php
/*
Widget Name: Cases
Description: 
Author: Pond
Author URI: 
*/

class Cases_theme_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'cases-theme-widget',
			__('Cases', 'custom-widget-bundle'),
			array(
				'description' => __('Cases', 'custom-widget-bundle'),
				'help'        => '',
			),
			array(
			
			),
			false,
			plugin_dir_path(__FILE__)
		);
	}
	
	function initialize() {
		$this->register_frontend_scripts(
			array(
				array(
					'carouFredSel',
					get_template_directory_uri().'/extend-widgets-bundle/extra-widgets/cases/js/jquery.carouFredSel-6.2.1-packed.js',
					array( 'jquery' ),
					'6.2.1'
				),
				array(
					'mousewheel',
					get_template_directory_uri().'/extend-widgets-bundle/extra-widgets/cases/js/helper-plugins/jquery.mousewheel.min.js',
					array( 'jquery' ),
					'3.0.6'
				),
				array(
					'touchSwipe',
					get_template_directory_uri().'/extend-widgets-bundle/extra-widgets/cases/js/helper-plugins/jquery.touchSwipe.min.js',
					array( 'jquery' ),
					'1.3.3'
				),
			)
		);
		/*
		$this->register_frontend_styles(
			array(
				array(
					'cases-style',
					get_template_directory_uri().'/extend-widgets-bundle/extra-widgets/cases/styles/cases.style', 
					array(), 
					'1.0'
				),
			)
		);
		*/
	}
	
	function get_widget_form() {
		return array(
			'cases_repeater' => array(
				'type' => 'repeater',
				'label' => __( 'A Cases repeater.' , 'custom-widget-bundle' ),
				'item_name'  => __( 'Cases item', 'custom-widget-bundle' ),
				'item_label' => array(
					'selector'     => "[id*='cases_repeat']",
					'update_event' => 'change',
					'value_method' => 'val'
				),
				'fields' => array(
					'cases_bg' => array(
						'type' => 'media',
						'label' => __( 'Cases background', 'custom-widget-bundle' ),
						'choose' => __( 'Choose cases background', 'custom-widget-bundle' ),
						'update' => __( 'Set image', 'custom-widget-bundle' ),
						'library' => 'image',
						'fallback' => true
					),
					'cases_logo' => array(
						'type' => 'media',
						'label' => __( 'Cases logo', 'custom-widget-bundle' ),
						'choose' => __( 'Choose cases logo', 'custom-widget-bundle' ),
						'update' => __( 'Set image', 'custom-widget-bundle' ),
						'library' => 'image',
						'fallback' => true
					),
					'cases_title' => array(
						'type' => 'text',
						'label' => __('Cases title', 'custom-widget-bundle'),
						'default' => ''
					),
					'cases_link' => array(
						'type' => 'link',
						'label' => __('Cases URL goes here', 'custom-widget-bundle'),
						'default' => ''
					)
				)
			)
		);
	}

    function get_template_name($instance) {
		return 'cases-tpl';
	}

	function get_template_dir($instance) {
		return 'tpl';
	}
	
	function get_style_name($instance) {
		return 'cases';
	}

}

siteorigin_widget_register('cases-theme-widget', __FILE__, 'Cases_theme_Widget');
?>