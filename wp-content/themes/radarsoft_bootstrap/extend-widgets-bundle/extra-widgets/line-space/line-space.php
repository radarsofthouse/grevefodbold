<?php
/*
Widget Name: Line Space
Description: 
Author: Pond
Author URI: 
*/

class Line_space_theme_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'line-space-theme-widget',
			__('Line space', 'custom-widget-bundle'),
			array(
				'description' => __('Line space', 'custom-widget-bundle'),
				'help'        => '',
			),
			array(
			
			),
			false,
			plugin_dir_path(__FILE__)
		);
	}
	
	function get_widget_form() {
		return array(
			'line_space_width' => array(
				'type' => 'number',
				'label' => __('Width value pixel', 'custom-widget-bundle'),
				'default' => ''
			),
			'line_space_height' => array(
				'type' => 'number',
				'label' => __('Height value pixel', 'custom-widget-bundle'),
				'default' => ''
			),
			'line_space_color' => array(
				'type' => 'color',
				'label' => __( 'Choose a color', 'custom-widget-bundle' ),
				'default' => ''
			)
		);
	}

    function get_template_name($instance) {
		return 'line-space-template';
	}

	function get_template_dir($instance) {
		return 'line-space-template';
	}
	
	function get_style_name($instance) {
		return 'line-space';
	}
	
	function get_less_variables( $instance ) {
		return array(
			'line_space_width' => $instance['line_space_width'].'px',
			'line_space_height' => $instance['line_space_height'].'px',
			'line_space_color' => $instance['line_space_color'],
		);
	}
}

siteorigin_widget_register('line-space-theme-widget', __FILE__, 'Line_space_theme_Widget');
?>