<?php
/*
Widget Name: Title Box
Description: 
Author: Pond
Author URI: 
*/

class Custom_title_box_theme_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'custom-title-box-theme-widget',
			__('Title Box', 'custom-widget-bundle'),
			array(
				'description' => __('Custom Title Box', 'custom-widget-bundle'),
				'help'        => '',
			),
			array(
			
			),
			false,
			plugin_dir_path(__FILE__)
		);
	}
	
	function get_widget_form() {
		return array(
			'title_box_text' => array(
				'type' => 'text',
				'label' => __('Title box text', 'custom-widget-bundle'),
				'default' => ''
			),
			'title_box_width' => array(
				'type' => 'number',
				'label' => __('Title box width', 'custom-widget-bundle'),
				'description' => __('px', 'custom-widget-bundle'),
				'default' => ''
			),
			'title_box_style' => array(
				'type' => 'section',
				'label' => __( 'Title Box Style' , 'widget-form-fields-text-domain' ),
				'hide' => true,
				'fields' => array(
					'title_box_style_text_color' => array(
						'type' => 'color',
						'label' => __( 'Text color', 'widget-form-fields-text-domain' )
					),
					'title_box_style_bg_color' => array(
						'type' => 'color',
						'label' => __( 'Background color', 'widget-form-fields-text-domain' )
					),
					'title_box_style_border_color' => array(
						'type' => 'color',
						'label' => __( 'Border color', 'widget-form-fields-text-domain' )
					)
				)
			),
			'title_box_padding' => array(
				'type' => 'section',
				'label' => __( 'Title Box Padding Style' , 'widget-form-fields-text-domain' ),
				'hide' => true,
				'fields' => array(
					'title_box_padding_top' => array(
						'type' => 'number',
						'label' => __( 'Padding Top & Bottom Pixel', 'widget-form-fields-text-domain' ),
					),
					'title_box_padding_left' => array(
						'type' => 'number',
						'label' => __( 'Padding Left & Right Pixel', 'widget-form-fields-text-domain' ),
					),
				)
			),
		);
	}

    function get_template_name($instance) {
		return 'title-box-tpl';
	}

	function get_template_dir($instance) {
		return 'tpl';
	}
	
	function get_style_name($instance) {
		return 'title-box-style';
	}
	
	function get_less_variables( $instance ) {
		$less_value = array();
		$less_value['title_box_style_text_color'] = $instance['title_box_style']['title_box_style_text_color'];
		$less_value['title_box_style_bg_color'] = $instance['title_box_style']['title_box_style_bg_color'];
		if($instance['title_box_width']!=''){
			$less_value['title_box_width'] = $instance['title_box_width'].'px';
		}else{
			$less_value['title_box_width'] = 'auto';
		}
		if($instance['title_box_style']['title_box_style_border_color']!=''){
			$less_value['title_box_style_border_size'] = '1px';
		}else{
			$less_value['title_box_style_border_size'] = '0px';
		}
		$less_value['title_box_style_border_color'] = $instance['title_box_style']['title_box_style_border_color'];

		if($instance['title_box_padding']['title_box_padding_top']!=''){
			$less_value['title_box_padding_top'] = $instance['title_box_padding']['title_box_padding_top'].'px';
		}else{
			$less_value['title_box_padding_top'] = '20px';
		}
		if($instance['title_box_padding']['title_box_padding_left']!=''){
			$less_value['title_box_padding_left'] = $instance['title_box_padding']['title_box_padding_left'].'px';
		}else{
			$less_value['title_box_padding_left'] = '60px';
		}
		
		return $less_value;
	}
}

siteorigin_widget_register('custom-title-box-theme-widget', __FILE__, 'Custom_title_box_theme_Widget');
?>