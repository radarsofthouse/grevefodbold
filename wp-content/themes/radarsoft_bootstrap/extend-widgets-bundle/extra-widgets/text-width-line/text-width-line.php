<?php
/*
Widget Name: Text with line
Description: 
Author: Pond
Author URI: 
*/

class Text_With_Line_theme_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'text-with-line-theme-widget',
			__('Text with line', 'custom-widget-bundle'),
			array(
				'description' => __('Text with line', 'custom-widget-bundle'),
				'help'        => '',
			),
			array(
			),
			false,
			plugin_dir_path(__FILE__)
		);
	}
	
	function get_widget_form() {
		return array(
			'text_width_line' => array(
				'type' => 'tinymce',
				'label' => __( 'Text editor area', 'custom-widget-bundle' ),
				'default' => '',
				'rows' => 10,
				'default_editor' => 'tinymce',
				'button_filters' => array(
					'mce_buttons' => array( $this, 'filter_mce_buttons' ),
					'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
					'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
					'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
					'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
				),
			),
			'text_line_width' => array(
				'type' => 'number',
				'label' => __('Width value pixel', 'custom-widget-bundle'),
				'default' => ''
			),
			'text_line_height' => array(
				'type' => 'number',
				'label' => __('Height value pixel', 'custom-widget-bundle'),
				'default' => ''
			),
			'text_line_color' => array(
				'type' => 'color',
				'label' => __( 'Choose a color', 'custom-widget-bundle' ),
				'default' => ''
			)
		);
	}

    function get_template_name($instance) {
		return 'text-width-line-template';
	}

	function get_template_dir($instance) {
		return 'text-width-line-template';
	}
	
	function get_style_name($instance) {
		return 'text-width-line';
	}
	
	function get_less_variables( $instance ) {
		return array(
			'text_line_width' => $instance['text_line_width'].'px',
			'text_line_height' => $instance['text_line_height'].'px',
			'text_line_color' => $instance['text_line_color'],
		);
	}
}

siteorigin_widget_register('text-with-line-theme-widget', __FILE__, 'Text_With_Line_theme_Widget');
?>