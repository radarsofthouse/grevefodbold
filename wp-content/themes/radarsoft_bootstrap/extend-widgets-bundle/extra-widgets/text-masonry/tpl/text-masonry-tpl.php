<?php
if(!empty($instance['text_masonry_repeater'])){
	?>
	<div id="text_masonry_<?php echo $instance['panels_info']['widget_id']; ?>" class="text-masonry">
		<?php
		foreach($instance['text_masonry_repeater'] as $index => $masonry){
			?>
			<div class="item">
				<?php
					if(!empty($masonry['text_masonry_editor'])){
						echo $masonry['text_masonry_editor'];
					}
				?>
			</div>
			<?php
		}
		?>
	</div>
	<script type="text/javascript" language="javascript">
		jQuery('#text_masonry_<?php echo $instance['panels_info']['widget_id']; ?>').masonry({
			itemSelector: '.item',
			/*isFitWidth: true,*/
			columnWidth: 100,
			isAnimated: true
		});
	</script>
	<?php
}
?>
