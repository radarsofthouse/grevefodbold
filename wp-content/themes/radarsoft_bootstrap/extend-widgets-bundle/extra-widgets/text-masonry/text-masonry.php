<?php
/*
Widget Name: Text editor masonry
Description:
Author: Pond
Author URI:
*/

class text_masonry_theme_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'text-masonary-theme-widget',
			__('Text editor Masonry', 'custom-widget-bundle'),
			array(
				'description' => __('Text editor Masonry', 'custom-widget-bundle'),
				'help'        => '',
			),
			array(

			),
			false,
			plugin_dir_path(__FILE__)
		);
	}

	function initialize() {
		$this->register_frontend_scripts(
			array(
				array(
					'text-masonry',
					get_template_directory_uri().'/extend-widgets-bundle/extra-widgets/text-masonry/js/jquery.masonry.min.js',
					array( 'jquery' ),
					'2.1.08'
				),
			)
		);
		$this->register_frontend_styles(
			array(
				array(
					'text-masonry-tpl',
					get_template_directory_uri().'/extend-widgets-bundle/extra-widgets/text-masonry/styles/text-masonry-tpl.css',
					array(),
					'1.0'
				),
			)
		);
	}

	function get_widget_form() {
		return array(
			'text_masonry_repeater' => array(
				'type' => 'repeater',
				'label' => __( 'Text masonry repeater.' , 'custom-widget-bundle' ),
				'item_name'  => __( 'Text masonry item', 'custom-widget-bundle' ),
				'item_label' => array(
					'selector'     => "[id*='text_masonry_title']",
					'update_event' => 'change',
					'value_method' => 'val'
				),
				'fields' => array(
					'text_masonry_title' => array(
				        'type' => 'text',
				        'label' => __('Title', 'custom-widget-bundle'),
				        'default' => ''
				    ),
					'text_masonry_editor' => array(
				        'type' => 'tinymce',
				        'label' => __( 'Visually edit.', 'custom-widget-bundle' ),
				        'default' => '',
				        'rows' => 10,
				        'default_editor' => 'html',
				        'button_filters' => array(
				            'mce_buttons' => array( $this, 'filter_mce_buttons' ),
				            'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
				            'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
				            'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
				            'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
				        ),
				    )
				)
			)
		);
	}

	function get_template_name($instance) {
		return 'text-masonry-tpl';
	}

	function get_template_dir($instance) {
		return 'tpl';
	}
}

siteorigin_widget_register('text-masonary-theme-widget', __FILE__, 'text_masonry_theme_Widget');
?>
