<?php
$post_selector_query = $instance['post_loop_filter_query_posts'];

$processed_query = siteorigin_widget_post_selector_process_query( $post_selector_query );

if(!$instance['post_loop_filter_showfilter']){
    ?>
    <div class="post-controllers-container">
    	<div id ="post-controllers-<?php echo $instance['_sow_form_id']; ?>" class="post-controllers wow fadeLeft text-center" data-wow-duration="1s" data-wow-delay=".1s">
    		<button type="button" class="filter-btn active" data-filter="all"><?php _e('All','radar_fd'); ?></button>
    		<?php
    		if($processed_query['tax_query']){
    			foreach ($processed_query['tax_query'] as $value) {
                    if(isset($value['terms'])){
                        $taxonomy = $value['taxonomy'];
                        $cat_name = get_term_by('slug', $value['terms'], $value['taxonomy']);
                        ?>
                        <button type="button" class="filter-btn" data-filter="<?php echo $value['terms']; ?>"><?php echo $cat_name->name; ?></button>
                        <?php
                    }
    			}

    		}
    		?>
    	</div>
    </div>
    <div class="mb-4"></div>
    <?php
}

$instance['post_loop_filter_column'];
if($instance['post_loop_filter_column']){
    $col = 12 / $instance['post_loop_filter_column'];
}

$query_result = new WP_Query( $processed_query );

if($query_result->have_posts()){
    ?><div id="filtr-container-<?php echo $instance['_sow_form_id']; ?>" class="row filtr-container"><?php
    while($query_result->have_posts()){
        $query_result->the_post();
            if($taxonomy){
                $post_cat_arr = array();
                $post_cats = get_the_terms(get_the_ID(), $taxonomy);
                foreach ($post_cats as $key => $value) {
                    $post_cat_arr[] = $value->slug;
                }
                $post_cat = implode(', ', $post_cat_arr);
            }
    		?>
    		<div class="card blog-feature blog-feature-style1 col-md-<?php echo $col; ?> col-sm-6 col-xs-12 filtr-item" data-category="<?php echo $post_cat; ?>">
    			<?php
    			if ( has_post_thumbnail() ) {
    				$thumb_url=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'blog-feature');
    				$thumb_url=$thumb_url[0];
    			}else{
    				if($radar_fd_option['no-img']['url']){
    					$thumb_url=$radar_fd_option['no-img']['url'];
    				}
    			}
    			if($thumb_url){
    				?>
    				<a href="<?php the_permalink(); ?>"><img class="card-img-top" src="<?php echo $thumb_url; ?>"></a>
    				<?php
    			}
    			?>
    			<div class="card-body">
    				<?php
    				if ( ! empty( $post_cats[0] ) ) {
        				echo '<span class="cat-name">'.esc_html( $post_cats[0]->name ).'</span>';
    				}
    				?>
    				<?php the_excerpt(); ?>
    			</div>
    			<div class="card-footer">
    				<div class="row">
    					<div class="col-8">
    						<div class="author"><?php echo get_the_author_meta('first_name').' '.get_the_author_meta('last_name'); ?></div>
    					</div>
    					<div class="col-4 text-right">
    						<div class="date"><?php echo get_the_date('j. M Y'); ?></div>
    					</div>
    				</div>
    			</div>
    		</div>
    		<?php
    	};
    	?>
    	</dvi>
    	<?php
    	if(function_exists('easy_wp_pagenavigation')){
    		echo easy_wp_pagenavigation();
    	}else{
    		if($query_result->max_num_pages > 1){
    			?>
    			<nav>
    				<ul class="pager">
    					<li><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'custom-widget-bundle' ) ); ?></li>
    					<li><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'custom-widget-bundle' ) ); ?></li>
    				</ul>
    			</nav>
    			<?php
    		}
    	}
        ?>
        <script>
        jQuery(document).ready(function(){
            jQuery('#filtr-container-<?php echo $instance['_sow_form_id']; ?>').filterizr({
                animationDuration: 0.5, // in seconds
                filter: 'all',
            });
            jQuery('#post-controllers-<?php echo $instance['_sow_form_id']; ?> button').click(function() {
                jQuery('#post-controllers-<?php echo $instance['_sow_form_id']; ?> button').removeClass('active');
                jQuery(this).addClass('active');
            });
        });
        </script>
        <?php
}else{
    ?>
	<h3><?php echo __( 'Nothing Found', 'custom-widget-bundle' ); ?></h3>
	<?php
}
?>
<?php wp_reset_postdata(); ?>
