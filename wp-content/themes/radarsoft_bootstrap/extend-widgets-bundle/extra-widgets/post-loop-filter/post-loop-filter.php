<?php
/*
Widget Name: Post loop filter
Description:
Author: Pond
Author URI:
*/

class Post_loop_filter_theme_Widget extends SiteOrigin_Widget {

	function __construct() {
		parent::__construct(
			'post-loop-filter-theme-widget',
			__('Post loop filter', 'custom-widget-bundle'),
			array(
				'description' => __('Post loop with filter data category', 'custom-widget-bundle'),
				'help'        => '',
			),
			array(

			),
			false,
			plugin_dir_path(__FILE__)
		);
	}

	function initialize() {
		$this->register_frontend_scripts(
			array(
				array(
					'filter',
					get_template_directory_uri().'/extend-widgets-bundle/extra-widgets/post-loop-filter/js/jquery.filterizr.min.js',
					array( 'jquery' ),
					'1',
					true
				),
			)
		);
	}

	function get_widget_form() {

		$templates = $this->get_loop_templates();
		if( empty($templates) ) {
			$list_templates = array();
		}else{
			$list_templates = array();
			foreach ($templates as $value) {
				$filename = explode('.',$value);
				$list_templates[$filename[0]] = $filename[0];
			}
		}

		return array(
			'post_loop_filter_title' => array(
				'type' => 'text',
				'label' => __('Title box text', 'custom-widget-bundle'),
				'default' => ''
			),
			'post_loop_filter_showfilter' => array(
		        'type' => 'checkbox',
		        'label' => __( 'Disable filter taxonomies', 'custom-widget-bundle' ),
		        'default' => false
		    ),
			'post_loop_filter_template' => array(
		        'type' => 'select',
		        'label' => __( 'Template', 'custom-widget-bundle' ),
		        'options' => $list_templates
		    ),
			'post_loop_filter_column' => array(
		        'type' => 'select',
		        'label' => __( 'Column', 'custom-widget-bundle' ),
		        'options' => array(
					'2' => __( 'Two', 'custom-widget-bundle' ),
					'3' => __( 'Three', 'custom-widget-bundle' ),
					'4' => __( 'Four', 'custom-widget-bundle' ),
					'6' => __( 'Six', 'custom-widget-bundle' )
				)
		    ),
			'post_loop_filter_query_posts' => array(
        		'type' => 'posts',
        		'label' => __('Posts query', 'custom-widget-bundle'),
    		),
		);
	}

	function get_template_name($instance) {
		return $instance['post_loop_filter_template'];
	}

	function get_template_dir($instance = '') {
		return 'tpl';
	}

	function get_loop_templates(){
		$dir = $this->get_template_dir();
		$folder = __DIR__.'/'.$dir;

		$templates = array_diff(scandir($folder), array('..', '.'));

		return $templates;
	}

	private function get_helper_widget( $templates ) {
		if ( empty( $this->helper ) &&
		     class_exists( 'SiteOrigin_Widget' ) &&
		     class_exists( 'SiteOrigin_Widget_Field_Posts' ) ) {
			$this->helper = new SiteOrigin_Panels_Widgets_PostLoop_Helper( $templates );
		}
		// These ensure the form fields name attributes are correct.
		$this->helper->id_base = $this->id_base;
		$this->helper->id = $this->id;
		$this->helper->number = $this->number;

		return $this->helper;
	}
	/*
    function get_template_name($instance) {
		return 'title-box-tpl';
	}

	function get_template_dir($instance) {
		return 'tpl';
	}

	function get_style_name($instance) {
		return 'title-box-style';
	}

	function get_less_variables( $instance ) {
		$less_value = array();
		$less_value['title_box_style_text_color'] = $instance['title_box_style']['title_box_style_text_color'];
		$less_value['title_box_style_bg_color'] = $instance['title_box_style']['title_box_style_bg_color'];
		if($instance['title_box_width']!=''){
			$less_value['title_box_width'] = $instance['title_box_width'].'px';
		}else{
			$less_value['title_box_width'] = 'auto';
		}
		if($instance['title_box_style']['title_box_style_border_color']!=''){
			$less_value['title_box_style_border_size'] = '1px';
		}else{
			$less_value['title_box_style_border_size'] = '0px';
		}
		$less_value['title_box_style_border_color'] = $instance['title_box_style']['title_box_style_border_color'];

		if($instance['title_box_padding']['title_box_padding_top']!=''){
			$less_value['title_box_padding_top'] = $instance['title_box_padding']['title_box_padding_top'].'px';
		}else{
			$less_value['title_box_padding_top'] = '20px';
		}
		if($instance['title_box_padding']['title_box_padding_left']!=''){
			$less_value['title_box_padding_left'] = $instance['title_box_padding']['title_box_padding_left'].'px';
		}else{
			$less_value['title_box_padding_left'] = '60px';
		}

		return $less_value;
	}
	*/
}

siteorigin_widget_register('post-loop-filter-theme-widget', __FILE__, 'Post_loop_filter_theme_Widget');
?>
