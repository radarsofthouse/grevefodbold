<div class="quotes">
	<div class="quotes-description">
		<?php echo $instance['quotes_description'] ?>
	</div>
	<div class="quotes-name-position">
		<div class="name"><?php echo $instance['quotes_name'] ?></div>
		<div class="position"><?php echo $instance['quotes_position'] ?></div>
	</div>
</div>