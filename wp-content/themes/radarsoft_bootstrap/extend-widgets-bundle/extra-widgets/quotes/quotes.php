<?php
/*
Widget Name: Quotes
Description: 
Author: Pond
Author URI: 
*/

class Quotes_theme_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'quotes-theme-widget',
			__('Quotes', 'custom-widget-bundle'),
			array(
				'description' => __('Quotes description with name and position', 'custom-widget-bundle'),
				'help'        => '',
			),
			array(
			
			),
			false,
			plugin_dir_path(__FILE__)
		);
	}
	
	function get_widget_form() {
		return array(
			'quotes_name' => array(
				'type' => 'text',
				'label' => __('Name', 'custom-widget-bundle'),
				'default' => ''
			),
			'quotes_position' => array(
				'type' => 'text',
				'label' => __('Position', 'custom-widget-bundle'),
				'default' => ''
			),
			'quotes_description' => array(
				'type' => 'tinymce',
				'label' => __( 'Quotes description', 'custom-widget-bundle' ),
				'default' => '',
				'rows' => 10,
				'default_editor' => 'tinymce',
				'button_filters' => array(
					'mce_buttons' => array( $this, 'filter_mce_buttons' ),
					'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
					'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
					'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
					'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
				),
			),
			'border_color' => array(
				'type' => 'color',
				'label' => __( 'Choose a border color', 'custom-widget-bundle' ),
				'default' => ''
			),
			'position_font_color' => array(
				'type' => 'color',
				'label' => __( 'Choose font color', 'custom-widget-bundle' ),
				'default' => ''
			)
		);
	}

    function get_template_name($instance) {
		return 'quotes-template';
	}

	function get_template_dir($instance) {
		return 'quotes-template';
	}
	
	function get_style_name($instance) {
		return 'quotes';
	}
	
	function get_less_variables( $instance ) {
		return array(
			'border_color' => $instance['border_color'],
			'position_font_color' => $instance['position_font_color'],
		);
	}
}

siteorigin_widget_register('quotes-theme-widget', __FILE__, 'Quotes_theme_Widget');
?>