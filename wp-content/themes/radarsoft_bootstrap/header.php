<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

<?php
	global $radar_fd_option;
	if($radar_fd_option['favicon']['url']){
		echo '<link rel="shortcut icon" type="image/x-icon" href="'.$radar_fd_option['favicon']['url'].'">';
	}
?>
</head>

<body <?php body_class(); ?>>

	<div class="site-header" itemscope itemtype="http://schema.org/WPHeader">
			<div class="container">
				<div class="nav-primary">
						<nav class="navbar navbar-expand-lg navbar-light">
							<?php
							if($radar_fd_option['logo']['url']){
								echo '
									<a href="'.home_url().'" class="navbar-brand">
										<img src="'.$radar_fd_option['logo']['url'].'">
									</a>
								';
							}
							?>
		  					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#primary-menu" aria-controls="primary-menu" aria-expanded="false" aria-label="Toggle navigation">
	    						<span class="navbar-toggler-icon"></span>
	  						</button>

							<div class="collapse navbar-collapse" id="primary-menu" itemscope itemtype="http://schema.org/SiteNavigationElement">
							<?php
								wp_nav_menu(array(
									'theme_location' => 'primary',
									'container' => 'ul',
									'menu_class'	=> 'navbar-nav ml-auto justify-content-end primary-menu',
									'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth' => 2,
									'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
									'walker' => new wp_bootstrap_navwalker()
								));
								if($radar_fd_option['enabled-top-search-icon']==1){
									?>
									<div class="d-sm-block d-md-block d-lg-none">
										<?php get_search_form(); ?>
									</div>
									<?php
								}
								?>
							</div>

							<?php
							if($radar_fd_option['enabled-top-search-icon']==1){
								?>
								<div class="header-search d-none d-lg-block">
									<a class="search-icon collapsed" data-toggle="collapse" href="#header-search-form" aria-expanded="false">
										<i class="fa fa-search" aria-hidden="true"></i>
									</a>
									<div id="header-search-form" class="header-search-form collapse">
										<?php get_search_form(); ?>
									</div>
								</div>
								<?php
							}
							?>
						</nav>
					</div>
			</div>
	</div>

	<div class="site-content" itemscope itemtype="http://schema.org/WebPage">
