<?php
global $radar_fd_option;
?>
	</div><!-- content -->

<footer class="site-footer" itemscope itemtype="http://schema.org/WPFooter">
	<div class="site-footer-info container">
		<div class="row">
			<div class="col-lg-4">
				<?php dynamic_sidebar('Footer Column 1'); ?>
			</div>
			<div class="col-lg-4">
				<?php dynamic_sidebar('Footer Column 2'); ?>
			</div>
			<div class="col-lg-4">
				<?php dynamic_sidebar('Footer Column 3'); ?>
			</div>
		</div>
	</div><!-- .site-footer-info -->
	<?php if($radar_fd_option['enabled-footer-copyright']==1){ ?>
		<div class="site-footer-copyright container-fluid">
			<?php echo $radar_fd_option['text-footer-copyright']; ?>
		</div>
	<?php } ?>
	<div id="ToTop" class="totop"><span class="icon-totop"></span><?php _e($radar_fd_option['to-top'],'radar_fd'); ?></div>
</footer><!-- #site-footer -->

<?php wp_footer(); ?>

</body>
</html>
