<?php
get_header(); ?>
	<div class="container">
		<h1><?php _e('Oops! That page can&rsquo;t be found.', 'radar_fd'); ?></h1>
		<p><?php _e('It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'radar_fd'); ?></p>
	</div>
<?php get_footer(); ?>
