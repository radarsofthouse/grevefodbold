<?php
get_header();

global $radar_fd_option;
	?>
	<div class="container">
		<?php
		the_archive_title('<h1 class="entry-title">', '</h1>');
		the_archive_description('<div class="taxonomy-description">', '</div>');
		?>
		<?php get_template_part('loops/loop', 'list'); ?>
	</div>
<?php get_footer(); ?>
