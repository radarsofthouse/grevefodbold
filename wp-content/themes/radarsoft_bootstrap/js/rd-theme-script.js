jQuery(document).ready(function(){
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() != 0) {
            jQuery('#ToTop').fadeIn();
        } else {
            jQuery('#ToTop').fadeOut();
        }
    });
    jQuery('#ToTop').click(function(){
        jQuery("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    var navbarToggle = '.navbar-toggler,';
    jQuery('.dropdown, .dropup').each(function() {
        var dropdown = jQuery(this),
        dropdownToggle = jQuery('[data-toggle="dropdown"]', dropdown),
        dropdownHoverAll = dropdownToggle.data('dropdown-hover-all') || false;

        // Mouseover
        dropdown.hover(function(){
            var notMobileMenu = jQuery(navbarToggle).size() > 0 && jQuery(navbarToggle).css('display') === 'none';
            if ((dropdownHoverAll == true || (dropdownHoverAll == false && notMobileMenu))) {
                dropdownToggle.trigger('click');
            }
        })
    });
});

jQuery(window).on("resize", function () {
    if(jQuery(window).width() <= 991){
        crow = 1;
        jQuery(".site-footer .widget").each(function(i,item){
            childel = jQuery(item).find('div').attr('class');
            check_title = jQuery(item).find('h3.widget-title').length;
            if(check_title!=0){
                jQuery(item).find('div').attr('id',childel+crow).addClass('collapse');
                jQuery(item).find('h3.widget-title').attr('data-toggle', 'collapse').attr('aria-expanded','false').attr('href','#'+childel+crow).addClass('toggle collapsed');
                crow+=1;
            }
        });
    }else{
        jQuery(".site-footer .widget").each(function(i,item){
            jQuery(item).find('div').removeClass('collapse');
            jQuery(item).find('h3.widget-title').removeAttr('data-toggle').removeAttr('href').removeClass('toggle collapsed');
        });
    }
}).resize();

jQuery('#header-search-form').on('shown.bs.collapse', function () {
    jQuery(this).find('input[type="search"]').focus();
})

jQuery('#header-search-form input[type="search"]').blur(function() {
    jQuery('#header-search-form').collapse('hide');
});
