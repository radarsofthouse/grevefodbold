<?php
if ( ! function_exists( 'radar_fd_setup' ) ) :
	function radar_fd_setup() {

		/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		*/
		load_theme_textdomain( 'radar_fd', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
		add_theme_support( 'title-tag' );

		/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		*/
		//add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'radar_fd' ),
		) );

		/*
		* Enable support for Post Formats.
		* See http://codex.wordpress.org/Post_Formats
		*/
		add_theme_support( 'post-formats', array(
			'aside', 'image', 'video', 'quote', 'link',
		) );

		add_theme_support( 'post-thumbnails' );

	}
endif; // radar_fd_setup
add_action( 'after_setup_theme', 'radar_fd_setup' );

/**
* Register widget area.
*
* @link http://codex.wordpress.org/Function_Reference/register_sidebar
*/
function radar_fd_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Header', 'radar_fd' ),
		'id'            => 'header',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'radar_fd' ),
		'id'            => 'sidebar',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Column 1', 'radar_fd' ),
		'id'            => 'footer-column1',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Column 2', 'radar_fd' ),
		'id'            => 'footer-column2',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Column 3', 'radar_fd' ),
		'id'            => 'footer-column3',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'radar_fd_widgets_init' );

/**
* Enqueue scripts and styles.
*/
function radar_fd_scripts() {
	wp_enqueue_style( 'bootstrap',get_template_directory_uri().'/css/bootstrap.css');
	wp_enqueue_script('bootstrap',get_template_directory_uri().'/js/bootstrap.js',array('jquery'),'',true);
	wp_enqueue_script('rd-theme-script',get_template_directory_uri().'/js/rd-theme-script.js',array('jquery'),'',true);
	wp_enqueue_style( 'style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'radar_fd_scripts' );

/**
* Load theme config file.
*/
if( file_exists(get_template_directory().'/inc/theme-config.php') ){
	require get_template_directory() . '/inc/theme-config.php';
}

if( file_exists(get_template_directory().'/inc/class-tgm-plugin-activation.php') ){
	require get_template_directory() . '/inc/class-tgm-plugin-activation.php';
	require get_template_directory() . '/inc/plugin_activation.php';
}
/*
if( file_exists(get_template_directory().'/inc/bs4navwalker.php') ){
	require get_template_directory() . '/inc/bs4navwalker.php';
}
*/
if( file_exists(get_template_directory().'/inc/wp_bootstrap_navwalker.php') ){
	require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';
}
if( file_exists(get_template_directory().'/extend-widgets-bundle/extend-widgets-bundle.php') ){
	require_once get_template_directory() . '/extend-widgets-bundle/extend-widgets-bundle.php';
}
