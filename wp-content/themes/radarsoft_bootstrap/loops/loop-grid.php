<?php
global $radar_fd_option;
if (have_posts()){
	?><div class="card-columns"><?php
	while (have_posts()){
		the_post();
		?>
		<div class="card">
				<?php
				if ( has_post_thumbnail() ) {
					$thumb_url=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
					$thumb_url=$thumb_url[0];
				}else{
					if($radar_fd_option['no-img']['url']){
						$thumb_url=$radar_fd_option['no-img']['url'];
					}else{
						$thumb_url=get_template_directory_uri().'/images/no_image.png';
					}
				}
				?>
				<img class="card-img-top" src="<?php echo $thumb_url; ?>">
				<div class="card-body">
					<h5 class="card-title"><?php the_title(); ?></h5>
					<p class="card-text"><?php the_excerpt(); ?></p>
				</div>
				<div class="card-footer text-right">
      				<small class="text-muted"><a href="<?php the_permalink(); ?>" class="btn btn-default"><?php echo __('Read more','radar_fd'); ?></a></small>
    			</div>
		</div>
	<?php
	};
	?>
	</div>
	<?php
	if(function_exists('easy_wp_pagenavigation')){
		echo easy_wp_pagenavigation();
	}else{
		if($wp_query->max_num_pages > 1){
			?>
			<nav>
				<ul class="pager">
					<li><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'radar_fd' ) ); ?></li>
					<li><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'radar_fd' ) ); ?></li>
				</ul>
			</nav>
			<?php
		}
	}
}else{
	?>
	<h3><?php echo __( 'Nothing Found', 'radar_fd' ); ?></h3>
	<?php
		if ( is_search() ) : ?>

			<p><?php echo __('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'radar_fd'); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php echo __('It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'radar_fd'); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	<?php
};
?>
<?php wp_reset_postdata(); ?>
