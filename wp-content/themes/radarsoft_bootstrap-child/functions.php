<?php
require_once(locate_template('/inc/theme-config.php'));

function enqueue_parent_theme_style() {
	wp_enqueue_style('fontawesome-style', 'https://use.fontawesome.com/releases/v5.0.6/css/all.css');
	wp_enqueue_style( 'parent-style', get_template_directory_uri() .'/style.css' );
	wp_enqueue_style('maincss', get_stylesheet_directory_uri().'/css/styles.css');
	wp_enqueue_style('fontawesome', get_stylesheet_directory_uri().'/assets/font-awesome-4.7.0/css/font-awesome.min.css');
	wp_enqueue_style( 'font-nunito', 'https://fonts.googleapis.com/css?family=Nunito' );

	wp_enqueue_script('filter',get_stylesheet_directory_uri().'/js/filter/jquery.filterizr.min.js',array('jquery'),'1',true);
	wp_enqueue_script('filter-script',get_stylesheet_directory_uri().'/js/filter/filter-script.js',array('jquery'),'',true);
}
add_action( 'wp_enqueue_scripts', 'enqueue_parent_theme_style');

add_action( 'after_setup_theme', 'child_theme_add_image_size' );
function child_theme_add_image_size() {
    add_image_size( 'blog-feature', 360, 160, true ); // (cropped)
}

function register_my_menus() {
	register_nav_menus(
		array(
			'left-menu' => __( 'Left Menu' ),
			'right-menu' => __( 'Right Menu' ),
			'mobile-menu' => __( 'Mobile Menu' ),
			'footer-menu-1' => __( 'Footer Menu Column One' ),
			'footer-menu-2' => __( 'Footer Menu Column Two' ),
			'footer-menu-3' => __( 'Footer Menu Column Three' ),
			'footer-menu-4' => __( 'Footer Menu Column Four' ),
			'footer-menu-5' => __( 'Footer Menu Column Five' ),
			'footer-menu-6' => __( 'Footer Menu Column Six' ),
		)
	);
}
add_action( 'init', 'register_my_menus' );

function widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Footer Column 4', 'radar_fd' ),
		'id'            => 'footer-column4',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Column 5', 'radar_fd' ),
		'id'            => 'footer-column5',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Column 6', 'radar_fd' ),
		'id'            => 'footer-column6',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Sub Footer Links & Address', 'radar_fd' ),
		'id'            => 'footer-sub-links',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'widgets_init', 11 );


add_filter( 'tribe-events-bar-filters',  'remove_search_from_bar', 1000, 1 );

function remove_search_from_bar( $filters ) {
  if ( isset( $filters['tribe-bar-date'] ) ) {
        unset( $filters['tribe-bar-date'] );
    }

    return $filters;
}

?>
