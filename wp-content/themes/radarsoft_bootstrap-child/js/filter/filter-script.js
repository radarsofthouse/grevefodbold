jQuery(document).ready(function(){
    jQuery('.filtr-container').filterizr({
        animationDuration: 0.5, // in seconds
        filter: 'all',
        controlsSelector: '.filter-btn',
        setupControls: true,
    });
    jQuery('.post-controllers button').click(function() {
        jQuery('.post-controllers button').removeClass('active');
        jQuery(this).addClass('active');
    });
});
