<?php
global $radar_fd_option;
if (have_posts()){
	while (have_posts()){
		the_post();
		?>
		<div class="card blog-feature blog-feature-style3">
			<div class="post-type"><?php echo get_post_type(); ?></div>
			<?php
			if ( has_post_thumbnail() ) {
				$thumb_url=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'blog-feature');
				$thumb_url=$thumb_url[0];
			}else{
				if($radar_fd_option['no-img']['url']){
					$thumb_url=$radar_fd_option['no-img']['url'];
				}
			}
			if($thumb_url){
				?>
				<a href="<?php the_permalink(); ?>"><img class="card-img-top" src="<?php echo $thumb_url; ?>"></a>
				<?php
			}
			?>
			<div class="card-body">
				<?php
				$categories = get_the_terms(get_the_ID(), 'partner_category');
				if ( ! empty( $categories ) ) {
    				echo '<span class="cat-name">'.esc_html( $categories[0]->name ).'</span>';
				}
				?>
				<?php the_excerpt(); ?>
			</div>
			<div class="card-footer">
				<div class="row">
					<div class="col-8">
						<div class="author"><?php echo get_the_author_meta('first_name').' '.get_the_author_meta('last_name'); ?></div>
					</div>
					<div class="col-4 text-right">
						<div class="date"><?php echo get_the_date('j. M Y'); ?></div>
					</div>
				</div>
			</div>
		</div>
		<?php if($wp_query->post_count!=1){ ?>
			<div class="mb-4"></div>
		<?php } ?>
		<?php
	};
}else{
	?>
	<h3><?php echo __( 'Nothing Found', 'radar_fd' ); ?></h3>
	<?php
		if ( is_search() ) : ?>
			<p><?php echo __('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'radar_fd'); ?></p>
		<?php else : ?>
			<p><?php echo __('It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'radar_fd'); ?></p>
		<?php endif; ?>
	<?php
}
?>
<?php wp_reset_postdata(); ?>
