<?php
global $radar_fd_option;

$categories = get_terms( array(
    'taxonomy' => 'category',
    'hide_empty' => false,
) );

?>
<div class="post-controllers-container">
	<div class="post-controllers wow fadeLeft text-center" data-wow-duration="1s" data-wow-delay=".1s">
		<button type="button" class="filter-btn active" data-filter="all"><?php _e('All','radar_fd'); ?></button>
		<?php
		if($categories){

			foreach ($categories as $key => $value) {
				?>
				<button type="button" class="filter-btn" data-filter="<?php echo $value->slug; ?>"><?php echo $value->name; ?></button>
				<?php
			}

		}
		?>
	</div>
</div>
<div class="mb-4"></div>
<?php

if (have_posts()){
	?><div class="row filtr-container"><?php
	while (have_posts()){
		the_post();
		$categories = get_the_category();
		?>
		<div class="card blog-feature blog-feature-style1 col-md-4 col-sm-6 filtr-item" data-category="<?php echo $categories[0]->slug; ?>">
			<?php
			if ( has_post_thumbnail() ) {
				$thumb_url=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'blog-feature');
				$thumb_url=$thumb_url[0];
			}else{
				if($radar_fd_option['no-img']['url']){
					$thumb_url=$radar_fd_option['no-img']['url'];
				}
			}
			if($thumb_url){
				?>
				<a href="<?php the_permalink(); ?>"><img class="card-img-top" src="<?php echo $thumb_url; ?>"></a>
				<?php
			}
			?>
			<div class="card-body">
				<?php
				if ( ! empty( $categories ) ) {
    				echo '<span class="cat-name">'.esc_html( $categories[0]->name ).'</span>';
				}
				?>
				<?php the_excerpt(); ?>
			</div>
			<div class="card-footer">
				<div class="row">
					<div class="col-8">
						<div class="author"><?php echo get_the_author_meta('first_name').' '.get_the_author_meta('last_name'); ?></div>
					</div>
					<div class="col-4 text-right">
						<div class="date"><?php echo get_the_date('j. M Y'); ?></div>
					</div>
				</div>
			</div>
		</div>
		<?php
	};
	?>
</div>
	<?php
	if(function_exists('easy_wp_pagenavigation')){
		echo easy_wp_pagenavigation();
	}else{
		if($wp_query->max_num_pages > 1){
			?>
			<nav>
				<ul class="pager">
					<li><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'radar_fd' ) ); ?></li>
					<li><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'radar_fd' ) ); ?></li>
				</ul>
			</nav>
			<?php
		}
	}
}else{
	?>
	<h3><?php echo __( 'Nothing Found', 'radar_fd' ); ?></h3>
	<?php
		if ( is_search() ) : ?>
			<p><?php echo __('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'radar_fd'); ?></p>
		<?php else : ?>
			<p><?php echo __('It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'radar_fd'); ?></p>
		<?php endif; ?>
	<?php
}
?>
<?php wp_reset_postdata(); ?>
