<?php if(!is_front_page()): ?>
<?php global $radar_fd_option; ?>
<footer class="site-footer">
	<div class="container">
		<div class="newsletter-box">
			<div class="row">
				<div class="col-md-7">
					<span class="newsletter-icon"></span>
					<p class="newsletter-desc"><?= __('Støt Greve Fodbold og alle vores værdier<br>Hold dig opdateret med vores nyhedsbrev*') ?></p>
				</div>
				<div class="col-md-5">
					<?php echo do_shortcode('[mc4wp_form id="356"]') ?>
				</div>
			</div>
		</div>

		<div class="row our-social-media">
			<div class="col-md-12">
				<strong><?= __('følg greve fodbold') ?></strong>
				<ul class="social-icons">
					<?php if(!empty($radar_fd_option['instagram-link'])): ?>
					<li><a href="<?= $radar_fd_option['instagram-link'] ?>"><i class="fab fa-instagram"></i></a></li>
					<?php endif; ?>

					<?php if(!empty($radar_fd_option['facebook-link'])): ?>
					<li><a href="<?= $radar_fd_option['facebook-link'] ?>"><i class="fab fa-facebook-f"></i></a></li>
					<?php endif; ?>

					<?php if(!empty($radar_fd_option['youtube-link'])): ?>
					<li><a href="<?= $radar_fd_option['youtube-link'] ?>"><i class="fab fa-youtube"></i></a></li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>

<div class="wrap-links">
	<div class="site-footer-info container">
		<div class="row">
			<div class="col-lg-2">
				<?php dynamic_sidebar('Footer Column 1'); ?>
			</div>
			<div class="col-lg-2">
				<?php dynamic_sidebar('Footer Column 2'); ?>
			</div>
			<div class="col-lg-2">
				<?php dynamic_sidebar('Footer Column 3'); ?>
			</div>
			<div class="col-lg-2">
				<?php dynamic_sidebar('Footer Column 4'); ?>
			</div>
			<div class="col-lg-2">
				<?php dynamic_sidebar('Footer Column 5'); ?>
			</div>
			<div class="col-lg-2">
				<?php dynamic_sidebar('Footer Column 6'); ?>
			</div>
		</div>
	</div>
</div>
<!-- .site-footer-info -->
<!--
	<div class="wrap-links">
		<div class="container links">
			<div class="row">
				<div class="col-md-12">
					<?php /*
					$defaults = array(
						'menu_class'      => 'footer-links',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'theme_location' => 'footer-menu',
					);
					wp_nav_menu( $defaults ); */
					?>
				</div>
			</div>
		</div>
	</div> -->
	<div class="container addr">
		<?php dynamic_sidebar( 'footer-sub-links' ); ?>

		<?php if(!empty($radar_fd_option['text-footer-copyright'])): ?>
		<div class="copyright"><?= $radar_fd_option['text-footer-copyright'] ?></div>
		<?php endif; ?>
	</div>
</footer>
<?php endif; ?>

<?php if(is_front_page()): ?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.totalpoll-choice-image').on('click', function(){
			jQuery(this).parent().find('input[name="totalpoll[choices][]"]').prop('checked', true);
			jQuery(this).closest('.totalpoll-poll-container').find('[name^="totalpoll[action]"][value="vote"]').click();
		});
	});
</script>
<?php endif; ?>

<?php wp_footer(); ?>

</body>
</html>
